// Get the geographic location from the browser
// Construct the query URL
// Use fetch to send the request to Flickr
// Process the response data into an object
// Use the values in the response object to construct an image source URL
// Display the first image on the page
// In response to some event (e.g. a button click or a setInterval), show the next image in the collection
const activate= document.getElementById("activate");

function constructImageURL (photoObj) {
  let num= randomizer()
  return "https://farm" + photoObj.photos.photo[num].farm +
  ".staticflickr.com/" + photoObj.photos.photo[num].server +
  "/" + photoObj.photos.photo[num].id + "_" + photoObj.photos.photo[num].secret + ".jpg";
}
//get URL
// navigator.geolocation.getCurrentPosition(function(position) {
  //     longitude = position.coords.longitude
  //     latitude = position.coords.latitude
  //   })
  let image= document.getElementById("image")
  
  const key = `e4f0982226706f2976c7d6f17c93e264`
  const queryURL = `https://cors-anywhere.herokuapp.com/https://flickr.com/services/rest/?api_key=${key}&format=json&nojsoncallback=1&method=flickr.photos.search&safe_search=1&per_page=5&lat=38.247613199999996&lon=-76.42158739999999&text=dog`
  function fetchREQ (){
    
    
    fetch(queryURL)
    .then((resp) => resp.json()) // Transform the data into json
    .then((photoObj)=> {
      console.log(photoObj)
      console.log(constructImageURL(photoObj)) 
      image.src= constructImageURL(photoObj)
      
    })}
    activate.onclick =fetchREQ

function randomizer (){
  return Math.floor(Math.random()*5)

}  
